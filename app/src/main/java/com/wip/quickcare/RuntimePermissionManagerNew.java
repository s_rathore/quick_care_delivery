package com.wip.quickcare;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;


public class RuntimePermissionManagerNew {

    private static final int REQUEST_READ_WRITE_PERMISSION = 1001;
    private AppCompatActivity activity;

    public RuntimePermissionManagerNew(@NonNull AppCompatActivity activity) {
        this.activity = activity;
    }

    public boolean areAllPermissionsGranted() {
        boolean allGranted = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
            };

            ArrayList<String> arr_denied_permissions = new ArrayList<>();
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(activity, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                        Toast.makeText(activity, "Please Allow All Permissions",
                                Toast.LENGTH_SHORT).show();
                    }
                    arr_denied_permissions.add(permission);
                }
            }
            //
            String[] denied_permissions = new String[arr_denied_permissions.size()];
            for (int i = 0; i < denied_permissions.length; i++) {
                denied_permissions[i] = arr_denied_permissions.get(i);
            }
            //
            if (denied_permissions.length > 0) {
                ActivityCompat.requestPermissions(activity, denied_permissions, REQUEST_READ_WRITE_PERMISSION);
            } else {
                allGranted = true;
            }
        } else {
            return true;
        }
        return allGranted;
    }

}
