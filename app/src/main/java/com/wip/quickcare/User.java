package com.wip.quickcare;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class User {

    public String user_name = "";
    public String phone_no = "";
    public String address = "";
    public String zipcode = "";
    public String image = "";
    public String order_quantity = "";
    public String order_status = "";

    public String first_name = "";
    public String last_name = "";
    public String email = "";
    public String mobile = "";
    public String aboutseetycontent1 = "";
    public String aboutseetycontent2 = "";
    public String aboutseetycontent3 = "";
    public String status="";
    public String full_name = "";
    public String password="";
    public String type="";
    public String profile_pic;
    public String error = "";
    public String message="";
    public String user_id = "";
    public String user_otp = "";
    public String formID ="";
    public String formType ="";

    public String calculationFormID ="";
    public String calculationFormFilterID ="";

    public String pdfUrl ="";

    public int id;
    public String status_code="";
    public String status_msg;
    public String viewings="";
    public static int newID;
    public int property_id;

    public String service_type ="";
    public String date ="";
    public String quotationNumber ="";

    public String time_start ="";
    public String time_end ="";
    public String latitude ="";
    public String longitude ="";
    public String problem_details ="";
    public String notification_booking_id="";
    public String service_img="";
    public String service_id ="";
    public String service_name ="";
    public String additionalCoverName ="";

    public String booking_status="";
    public String tech_fullname="";
    public String tech_phone ="";
    public String tech_image ="";

    public String zoneName ="";
    public String make ="";
    public String model ="";
    public String seatingCapacity ="";
    public String seating_capacity ="";

    public String registrationYears ="";

    public String addOnCoverName ="";
    public String policyType ="";
    public String discount ="";
    public String imt23 ="";
    public String varient ="";
    public String cityName ="";
    public String occupancyName ="";
    public String specializationName ="";

    public String vehicle_company ="";
    public String insurer_company ="";

    public String amount ="";
    public String basic_od ="";
    public String elec_accessories ="";
    public String bi_fuel_kit ="";

    public String sumInsured ="";
    public String vehicle_company_image ="";
    public String idv_amount ="";
    public String showroom_price ="";
    public String cubic_capacity ="";

    public String fuel_type ="";
    public String discounts ="";
    public String anti_theft ="";
    public String basic_tp ="";
    public String basic_tp2 ="";

    public String trailer ="";
    public String compulsory_pa_own ="";
    public String unnamed_pa ="";
    public String ll_person_operation ="";
    public String ll_drivers ="";

    public String no_person ="";
    public String ll_non_fare ="";
    public String no_person1 ="";
    public String total_tp_premium ="";
    public String total_premium ="";

    public String gst_amount ="";
    public String total_payable ="";
    public String ncb_amount ="";
    public String od_discount ="";
    public String od_loading ="";
    public String tp_bi_fuel_kit ="";
    public String tp_geog_area ="";

    public String discount_total ="";
    public String addon_total ="";
    public String add_on_cover ="";

    public String age_year ="";
    public String geog_area ="";
    public String fibre_glass_tank ="";
    public String imt_23_cover ="";
    public String total_annual_od_premium ="";
    public String ncb ="";
    public String ncb1 ="";

    public String insurer_id ="";
    public String insurer_name ="";
    public String logo ="";
    public String particular ="";
    public String occupancy_rate ="";
    public String total_occupancy ="";
    public String eq_rate ="";
    public String total_eq ="";
    public String stfi_rate ="";
    public String total_stfi ="";
    public String net_premium ="";
    public String gst ="";
    public String sum_insured ="";
    public String final_premium ="";

    public String total_od_premium ="";
    public String insurerName = "";

    public String total_employees ="";
    public String age_category = "";

    public static ArrayList<String> insurerDetailsList = new ArrayList<String>();
    public static ArrayList<User> bookinghistoryList = new ArrayList<>();
    public static ArrayList<User> allServicesList = new ArrayList<>();
    public static ArrayList<User> allBannerList = new ArrayList<>();
    public static ArrayList<String> additionalCoverNameList = new ArrayList<String>();

    public static ArrayList<String> rtoZoneDetailsList = new ArrayList<String>();
    public static ArrayList<String> coverageTypeDetailsList = new ArrayList<String>();

    public static ArrayList<String> ageGroupDetailsList = new ArrayList<String>();
    public static ArrayList<String> groupSumInsuredDetailsList = new ArrayList<String>();

    public static ArrayList<String> cityList = new ArrayList<String>();
    public static ArrayList<String> occupancyList = new ArrayList<String>();
    public static ArrayList<String> riskCategoryList = new ArrayList<String>();

    public static ArrayList<String> sumInsuredList = new ArrayList<String>();
    public static ArrayList<String> doctorPolicyTypeList = new ArrayList<String>();
    public static ArrayList<String> juridctionList = new ArrayList<String>();

    public static ArrayList<String> manufecturerVehicleDetailsList = new ArrayList<String>();
    public static ArrayList<String> modelDetailsList = new ArrayList<String>();
    public static ArrayList<String> varientDetailsList = new ArrayList<String>();
    public static ArrayList<String> multipleAddonsGroupHealth = new ArrayList<String>();


    public static ArrayList<String> seatingDetailsList = new ArrayList<String>();
    public static ArrayList<String> registrationYearDetailsList = new ArrayList<String>();
    public static ArrayList<String> imt23DetailsList = new ArrayList<String>();
    public static ArrayList<String> policyTypeList = new ArrayList<String>();
    public static ArrayList<String> addOnCoverNameList = new ArrayList<String>();
    public static ArrayList<String> discountTypeList = new ArrayList<String>();
    public static ArrayList<String> sumInsuredPaidDriverList = new ArrayList<String>();

    public static ArrayList<User> allInsurQuotationPlanList = new ArrayList<>();
    public static ArrayList<User> allInsurerNameList = new ArrayList<>();
    public static ArrayList<User> allGroupHealthInsurerList = new ArrayList<>();
    public static ArrayList<User> fireBurglaryHistoryList = new ArrayList<>();
    private String value ="";
    private String typeName ="";


    public void getInsurerDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("insurers")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("insurers");
                    Log.e("insurer_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User ncblist = new User();
                        JSONObject insurerlistHistory = jsonArray.optJSONObject(i);
                        insurerName           = insurerlistHistory.optString("name");
                        insurerDetailsList.add(insurerName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void parseFireBurglaryList(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");


                if (jsonObject.has("searchData")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("searchData");
                    Log.e("searchData_size", "" + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User fireBurglarylist = new User();
                        JSONObject NearbyObjectFireBurglary = jsonArray.optJSONObject(i);

                        fireBurglarylist.insurer_id = NearbyObjectFireBurglary.optString("insurer_id");
                        fireBurglarylist.insurer_name = NearbyObjectFireBurglary.optString("insurer_name");
                        fireBurglarylist.logo = NearbyObjectFireBurglary.optString("logo");
                        fireBurglarylist.particular = NearbyObjectFireBurglary.optString("particular");
                        fireBurglarylist.occupancy_rate = NearbyObjectFireBurglary.optString("occupancy_rate");
                        fireBurglarylist.total_occupancy = NearbyObjectFireBurglary.optString("total_occupancy");
                        fireBurglarylist.eq_rate = NearbyObjectFireBurglary.optString("eq_rate");
                        fireBurglarylist.total_eq = NearbyObjectFireBurglary.optString("total_eq");
                        fireBurglarylist.stfi_rate = NearbyObjectFireBurglary.optString("stfi_rate");
                        fireBurglarylist.total_stfi = NearbyObjectFireBurglary.optString("total_stfi");
                        fireBurglarylist.net_premium = NearbyObjectFireBurglary.optString("net_premium");
                        fireBurglarylist.gst = NearbyObjectFireBurglary.optString("gst");
                        fireBurglarylist.sum_insured = NearbyObjectFireBurglary.optString("sum_insured");
                        fireBurglarylist.final_premium = NearbyObjectFireBurglary.optString("final_premium");

                        fireBurglaryHistoryList.add(fireBurglarylist);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void parseGroupHealthInsurerListDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");

                if (jsonObject.has("list")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("list");
                    Log.e("list_size", "" + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User groupHealthInsurerlist = new User();
                        JSONObject nearbyObjectGroupHealthInsurer = jsonArray.optJSONObject(i);

                        groupHealthInsurerlist.insurer_id = nearbyObjectGroupHealthInsurer.optString("id");
                        groupHealthInsurerlist.sum_insured = nearbyObjectGroupHealthInsurer.optString("sum_insured");
                        groupHealthInsurerlist.total_employees = nearbyObjectGroupHealthInsurer.optString("total_employees");
                        groupHealthInsurerlist.age_category = nearbyObjectGroupHealthInsurer.optString("age_category");

                        allGroupHealthInsurerList.add(groupHealthInsurerlist);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void parseInsurQuotationPlanListDetailsNew(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");

                    if (jsonObject.has("insurer")) {
                        JSONArray jsonArray1 = jsonObject.optJSONArray("insurer");
                        Log.e("insurar_size", "" + jsonArray1.length());
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            User insurerNamelist = new User();
                            JSONObject NearbyObjectinsurerName = jsonArray1.optJSONObject(i);

                            insurerNamelist.insurer_company = NearbyObjectinsurerName.optString("insurer_company");

                            allInsurerNameList.add(insurerNamelist);
                        }
                    }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }




    public void parseInsurQuotationPlanListDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
                zoneName = jsonObject.optString("rto_name");
                make = jsonObject.optString("make");
                model = jsonObject.optString("model");
                varient = jsonObject.optString("variant");
                date = jsonObject.optString("registered_date");
                quotationNumber= jsonObject.optString("quotation_number");


                if (jsonObject.has("insurance_data")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("insurance_data");
                    Log.e("json_insuranc_size", "" + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User quotationPlanlist = new User();
                        JSONObject NearbyObjectQuotationPlan = jsonArray.optJSONObject(i);

                        quotationPlanlist.vehicle_company = NearbyObjectQuotationPlan.optString("vehicle_company");
                        quotationPlanlist.vehicle_company_image = NearbyObjectQuotationPlan.optString("vehicle_company_image");

                        quotationPlanlist.showroom_price = NearbyObjectQuotationPlan.optString("showroom_price");
                        quotationPlanlist.cubic_capacity = NearbyObjectQuotationPlan.optString("cubic_capacity");
                        quotationPlanlist.fuel_type = NearbyObjectQuotationPlan.optString("fuel_type");
                        quotationPlanlist.idv_amount = NearbyObjectQuotationPlan.optString("idv_amount");
                        quotationPlanlist.amount = NearbyObjectQuotationPlan.optString("amount");
                        quotationPlanlist.basic_od = NearbyObjectQuotationPlan.optString("basic_od");
                        quotationPlanlist.elec_accessories = NearbyObjectQuotationPlan.optString("elec_accessories");
                        quotationPlanlist.bi_fuel_kit = NearbyObjectQuotationPlan.optString("bi_fuel_kit");
                        quotationPlanlist.geog_area = NearbyObjectQuotationPlan.optString("geog_area");
                        quotationPlanlist.fibre_glass_tank = NearbyObjectQuotationPlan.optString("fibre_glass_tank");
                        quotationPlanlist.imt_23_cover = NearbyObjectQuotationPlan.optString("imt_23_cover");
                        quotationPlanlist.total_annual_od_premium = NearbyObjectQuotationPlan.optString("total_annual_od_premium");
                        quotationPlanlist.total_od_premium = NearbyObjectQuotationPlan.optString("total_od_premium");
                        quotationPlanlist.ncb = NearbyObjectQuotationPlan.optString("ncb");
                        quotationPlanlist.ncb1 = NearbyObjectQuotationPlan.optString("ncb1");
                        quotationPlanlist.ncb_amount = NearbyObjectQuotationPlan.optString("ncb_amount");
                        quotationPlanlist.discounts = NearbyObjectQuotationPlan.optString("discounts");
                        quotationPlanlist.anti_theft = NearbyObjectQuotationPlan.optString("anti_theft");
                        quotationPlanlist.od_discount = NearbyObjectQuotationPlan.optString("od_discount");
                        quotationPlanlist.od_loading = NearbyObjectQuotationPlan.optString("od_loading");
                        quotationPlanlist.basic_tp = NearbyObjectQuotationPlan.optString("basic_tp");
                        quotationPlanlist.trailer = NearbyObjectQuotationPlan.optString("trailer");

                        quotationPlanlist.tp_bi_fuel_kit = NearbyObjectQuotationPlan.optString("tp_bi_fuel_kit");
                        quotationPlanlist.tp_geog_area = NearbyObjectQuotationPlan.optString("tp_geog_area");

                        quotationPlanlist.age_year = NearbyObjectQuotationPlan.optString("age_year");
                        quotationPlanlist.seating_capacity = NearbyObjectQuotationPlan.optString("seating_capacity");

                        quotationPlanlist.compulsory_pa_own = NearbyObjectQuotationPlan.optString("compulsory_pa_own");
                        quotationPlanlist.unnamed_pa = NearbyObjectQuotationPlan.optString("unnamed_pa");
                        quotationPlanlist.ll_person_operation = NearbyObjectQuotationPlan.optString("ll_person_operation");
                        quotationPlanlist.ll_drivers = NearbyObjectQuotationPlan.optString("ll_drivers");
                        quotationPlanlist.no_person = NearbyObjectQuotationPlan.optString("no_person");
                        quotationPlanlist.ll_non_fare = NearbyObjectQuotationPlan.optString("ll_non_fare");

                        quotationPlanlist.no_person1 = NearbyObjectQuotationPlan.optString("no_person1");
                        quotationPlanlist.basic_tp2 = NearbyObjectQuotationPlan.optString("basic_tp2");
                        quotationPlanlist.total_tp_premium = NearbyObjectQuotationPlan.optString("total_tp_premium");

                        quotationPlanlist.total_premium = NearbyObjectQuotationPlan.optString("total_premium");
                        quotationPlanlist.gst_amount = NearbyObjectQuotationPlan.optString("gst_amount");
                        quotationPlanlist.total_payable = NearbyObjectQuotationPlan.optString("total_payable");


                        quotationPlanlist.discount_total = NearbyObjectQuotationPlan.optString("discount_total");
                        quotationPlanlist.add_on_cover = NearbyObjectQuotationPlan.optString("add_on_cover");
                        quotationPlanlist.addon_total = NearbyObjectQuotationPlan.optString("addon_total");
                        // quotationPlanlist.service_name               = NearbyObjectQuotationPlan.optString("category_name");

                        allInsurQuotationPlanList.add(quotationPlanlist);
                    }

                    if (jsonObject.has("insurer")) {
                        JSONArray jsonArray1 = jsonObject.optJSONArray("insurer");
                        Log.e("insurar_size", "" + jsonArray1.length());
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            User insurerNamelist = new User();
                            JSONObject NearbyObjectinsurerName = jsonArray1.optJSONObject(i);

                            insurerNamelist.insurer_company = NearbyObjectinsurerName.optString("insurer_company");

                            allInsurerNameList.add(insurerNamelist);
                        }
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getMultipleAddonsGroupHealthDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("addons")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("addons");
                    Log.e("addons_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject multipleAddonsGroupHealthHistory = jsonArray.optJSONObject(i);
                        value           = multipleAddonsGroupHealthHistory.optString("value");
                        multipleAddonsGroupHealth.add(value);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void getPolicyTypeDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("policy_types")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("policy_types");
                    Log.e("policyType_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject policyTypeHistory = jsonArray.optJSONObject(i);
                        policyType           = policyTypeHistory.optString("policy_type");
                        policyTypeList.add(policyType);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void getImtDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("imts_23")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("imts_23");
                    Log.e("imt_23_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject imtlistHistory = jsonArray.optJSONObject(i);
                        imt23           = imtlistHistory.optString("imt_23");
                        imt23DetailsList.add(imt23);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void getdiscountTypeDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("discounts")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("discounts");
                    Log.e("discount_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject discountlistHistory = jsonArray.optJSONObject(i);
                        discount           = discountlistHistory.optString("discount");
                        discountTypeList.add(discount);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getpaCoverPaidDriverDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("sum_insured")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("sum_insured");
                    Log.e("sum_insured_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject sumInsuredlistHistory = jsonArray.optJSONObject(i);
                        sumInsured           = sumInsuredlistHistory.optString("sum_insured");
                        sumInsuredPaidDriverList.add(sumInsured);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void getSeatingCapacityDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("capacity")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("capacity");
                    Log.e("seating_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject seatinglistHistory = jsonArray.optJSONObject(i);
                        seatingCapacity           = seatinglistHistory.optString("seating_capacity");
                        seatingDetailsList.add(seatingCapacity);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void parseLoginUser(JSONObject LoginObject) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(LoginObject));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message=jsonObject.optString("message");
                user_id = jsonObject.optString("user_id");
                user_otp = jsonObject.optString("user_otp");
                formID   = jsonObject.optString("form_id");
                formType   = jsonObject.optString("form_type");
                calculationFormID   = jsonObject.optString("calculation_form_id");
                calculationFormFilterID   = jsonObject.optString("calculation_form_filter_id");
                pdfUrl = jsonObject.optString("pdf_url");
               // user_id":4,"user_otp":712143
                Log.e("status",status_code+message+user_id);
            /*if (jsonObject.has("data")) {
                JSONObject userObject = jsonObject.optJSONObject("data");
                user_name = userObject.optString("user_name");
                first_name = userObject.optString("first_name");
                last_name = userObject.optString("last_name");
                full_name = userObject.optString("full_name");
                email = userObject.optString("email");
                mobile = userObject.optString("mobile");
                password = userObject.optString("password");
                id = userObject.optInt("user_id");
            }*/
            }
            else {
                if (jsonObject.has("status_code")){

                    status_code = jsonObject.optString("status_code");
                    message=jsonObject.optString("message");
                    Log.e("status12",status+message);
                }
                if (jsonObject.has("error")) {
                    error = jsonObject.optString("error");
                    message = jsonObject.optString("message");
                    Log.e("status_error",error+message);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void getManufecturerVehicleDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("vehicles")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("vehicles");
                    Log.e("manufecturer_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject manufecturerVehiclelistHistory = jsonArray.optJSONObject(i);
                        make           = manufecturerVehiclelistHistory.optString("make");
                        manufecturerVehicleDetailsList.add(make);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getVehicleModelDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("models")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("models");
                    Log.e("model_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject vehicleModellistHistory = jsonArray.optJSONObject(i);
                        model           = vehicleModellistHistory.optString("model");
                        modelDetailsList.add(model);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void getVarientDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("variants")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("variants");
                    Log.e("Varient_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject variantModellistHistory = jsonArray.optJSONObject(i);
                        varient           = variantModellistHistory.optString("variant");
                        varientDetailsList.add(varient);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getRegistrationYearDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("years")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("years");
                    Log.e("RegistrationYear_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject registrationYearlistHistory = jsonArray.optJSONObject(i);
                        registrationYears           = registrationYearlistHistory.optString("year");
                        registrationYearDetailsList.add(registrationYears);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getcityNameDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("cities")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("cities");
                    Log.e("cities_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject citylistHistory = jsonArray.optJSONObject(i);
                        cityName           = citylistHistory.optString("city_name");
                        cityList.add(cityName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSumInsuredDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("limits")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("limits");
                    Log.e("sumInsured_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject sumInsuredlistHistory = jsonArray.optJSONObject(i);
                        value           = sumInsuredlistHistory.optString("value");
                        sumInsuredList.add(value);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getDoctorPolicyTypeDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("policy_types")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("policy_types");
                    Log.e("policy_types_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject policyTypeslistHistory = jsonArray.optJSONObject(i);
                        typeName           = policyTypeslistHistory.optString("type_name");
                        doctorPolicyTypeList.add(typeName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getJuriductionNameDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("territories")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("territories");
                    Log.e("territories_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject territorieslistHistory = jsonArray.optJSONObject(i);
                        value           = territorieslistHistory.optString("value");
                        juridctionList.add(value);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getRiskCategoryNameDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("specializations")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("specializations");
                    Log.e("riskCategory_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject riskCategorylistHistory = jsonArray.optJSONObject(i);
                        specializationName           = riskCategorylistHistory.optString("specialization_name");
                        riskCategoryList.add(specializationName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void getOccupancyNameDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("occupancies")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("occupancies");
                    Log.e("occupancies_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject occupancylistHistory = jsonArray.optJSONObject(i);
                        occupancyName           = occupancylistHistory.optString("occupancy_name");
                        occupancyList.add(occupancyName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getRtoZoneDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("zones")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("zones");
                    Log.e("rtoZone_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject rtoZonelistHistory = jsonArray.optJSONObject(i);
                        zoneName           = rtoZonelistHistory.optString("zone_name");
                        rtoZoneDetailsList.add(zoneName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getCoverageTypeDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("types")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("types");
                    Log.e("coverageType_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject coverageTypelistHistory = jsonArray.optJSONObject(i);
                        value           = coverageTypelistHistory.optString("value");
                        coverageTypeDetailsList.add(value);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getAgeGroupDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("empcategory")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("empcategory");
                    Log.e("empcategory_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject empcategorylistHistory = jsonArray.optJSONObject(i);
                        value           = empcategorylistHistory.optString("value");
                        ageGroupDetailsList.add(value);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void getGroupHealthSumInsuredDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("limits")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("limits");
                    Log.e("sumInsured_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject coverageTypelistHistory = jsonArray.optJSONObject(i);
                        value           = coverageTypelistHistory.optString("value");
                        groupSumInsuredDetailsList.add(value);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }




    public void parseForgotPassword(JSONObject ForgotPasswordObject){
        try {
            if (ForgotPasswordObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = ForgotPasswordObject.optString("status_code");
                message = ForgotPasswordObject.optString("message");
                password = ForgotPasswordObject.optString("userPassword");
                user_otp = ForgotPasswordObject.optString("userOTP");
                user_id = ForgotPasswordObject.optString("user_id");

            }
            else {
                if (ForgotPasswordObject.has("status_code")){
                    status_code = ForgotPasswordObject.optString("status_code");
                    message=ForgotPasswordObject.optString("message");
                }

                if (ForgotPasswordObject.has("error"))
                    error = ForgotPasswordObject.optString("error");
                message=ForgotPasswordObject.optString("message");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getadditionalCoverDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("additional_covers")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("additional_covers");
                    Log.e("additional_cover_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User ncblist = new User();
                        JSONObject additionalCoverlistHistory = jsonArray.optJSONObject(i);
                        additionalCoverName           = additionalCoverlistHistory.optString("additional_cover");
                        additionalCoverNameList.add(additionalCoverName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getaddOnCoverDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message     = jsonObject.optString("message");

                if (jsonObject.has("add_ons")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("add_ons");
                    Log.e("add_on_cover_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User ncblist = new User();
                        JSONObject addOnCoverlistHistory = jsonArray.optJSONObject(i);
                        addOnCoverName           = addOnCoverlistHistory.optString("add_on");
                        addOnCoverNameList.add(addOnCoverName);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void parseSaveDetails(JSONObject LoginObject) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(LoginObject));
            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                status = jsonObject.optString("status");
                message=jsonObject.optString("status_msg");

                Log.e("status",status+message);
            }
            else {
                if (jsonObject.has("status")){

                    status = jsonObject.optString("status");
                    message=jsonObject.optString("message");
                    Log.e("status12",status+message);
                }
                if (jsonObject.has("error"))
                    error = jsonObject.optString("error");
                message=jsonObject.optString("message");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void parseProfileUser(JSONObject profileObject) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(profileObject));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message=jsonObject.optString("message");
              //  if (jsonObject.has("data")) {
                   // JSONObject userObject = jsonObject.optJSONObject("data");

                    user_name = jsonObject.optString("user_name");
                    email = jsonObject.optString("user_email");
                    mobile = jsonObject.optString("user_mobile");
                    password   = jsonObject.optString("password");
                    profile_pic = jsonObject.optString("profile_pic");
               // }
            }
            else {
                if (jsonObject.has("status_code")){
                    status_code = jsonObject.optString("status_code");
                    message=jsonObject.optString("message");
                }

                if (jsonObject.has("error"))
                    error = jsonObject.optString("error");
                    message=jsonObject.optString("message");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void parseAboutUsDetails(JSONObject aboutObject) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(aboutObject));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");

                mobile               = jsonObject.optString("contact_phone");
                email                = jsonObject.optString("contact_email");
                address              = jsonObject.optString("contact_address");
                aboutseetycontent1   = jsonObject.optString("about_content");

            }
            else {
                if (jsonObject.has("status_code")){
                    status_code = jsonObject.optString("status_code");
                }

                if (jsonObject.has("error"))
                    error = jsonObject.optString("error");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }





    /*{"status":"true","message":"Get Booking Detail Successfully.","data":[{"service_type":"electrician",
            "address":"6\/99, Hans Vihar, Sector 2, Mansarovar, Jaipur, Rajasthan 302020, India",
            "date":"2019-03-08",
            "time_start":"10:41 AM",
            "time_end":"11:55 AM",
            "latitude":"26.836609823414932",
            "longitude":"75.77046308666468",
            "problem_details":"Test ",
            "booking_status":"open",
            "tech_fullname":"dusyant",
            "tech_phone":"9874562508",
            "tech_image":"http:\/\/infocox.com\/insurancecalculator\/files\/assets\/images\/users\/image1555064510812.png"}}*/


    public void parseNotificationBookingHistory(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                status = jsonObject.optString("status");
                message = jsonObject.optString("message");

                if (jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("data");
                    Log.e("json arry size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User orderhistory = new User();
                        JSONObject NearbyObject = jsonArray.optJSONObject(i);

                        orderhistory.service_type               = NearbyObject.optString("service_type");
                        orderhistory.date                       = NearbyObject.optString("date");
                        orderhistory.address                    = NearbyObject.optString("address");
                        orderhistory.time_start                 = NearbyObject.optString("time_start");
                        orderhistory.time_end                   = NearbyObject.optString("time_end");
                        orderhistory.problem_details            = NearbyObject.optString("problem_details");
                        orderhistory.latitude                   = NearbyObject.optString("latitude");
                        orderhistory.longitude                  = NearbyObject.optString("longitude");
                        orderhistory.booking_status             = NearbyObject.optString("booking_status");
                        orderhistory.tech_fullname              = NearbyObject.optString("tech_fullname");
                        orderhistory.tech_phone                 = NearbyObject.optString("tech_phone");
                        orderhistory.tech_image                 = NearbyObject.optString("tech_image");
                        orderhistory.notification_booking_id    = NearbyObject.optString("notification_booking_id");

                        bookinghistoryList.add(orderhistory);
                    }
                }
            }else {
                status = jsonObject.optString("status");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void parseItemBookingDetails(JSONObject aboutObject) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(aboutObject));
            if (jsonObject.optString("status").equalsIgnoreCase("true")) {
                status = jsonObject.optString("status");
                message=jsonObject.optString("message");

                service_type            = jsonObject.optString("service_type");
                date                    = jsonObject.optString("date");
                address                 = jsonObject.optString("address");
                time_start              = jsonObject.optString("time_start");
                time_end                = jsonObject.optString("time_end");
                problem_details         = jsonObject.optString("problem_details");
                booking_status          = jsonObject.optString("booking_status");
                tech_fullname           = jsonObject.optString("tech_fullname");
                tech_phone              = jsonObject.optString("tech_phone");
                tech_image              = jsonObject.optString("tech_image");
                notification_booking_id = jsonObject.optString("booking_id");

            }
            else {
                if (jsonObject.has("status")){
                    status = jsonObject.optString("status");
                    message=jsonObject.optString("message");
                }

                if (jsonObject.has("error"))
                    error = jsonObject.optString("error");
                    message=jsonObject.optString("message");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void parseBannerListDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");

                if (jsonObject.has("banners")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("banners");
                    Log.e("json_banners_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User bannerlist = new User();
                        JSONObject NearbyObjectServices = jsonArray.optJSONObject(i);

                        bannerlist.service_img                = NearbyObjectServices.optString("FilePath");
                        bannerlist.service_id                 = NearbyObjectServices.optString("category_id");
                        bannerlist.service_name               = NearbyObjectServices.optString("title");

                        allBannerList.add(bannerlist);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public void parseVehicleCategoryListDetails(JSONObject jsonObject1)
    {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(jsonObject1));
            if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");

                if (jsonObject.has("categories")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("categories");
                    Log.e("json_categories_size",""+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        User servicelist = new User();
                        JSONObject NearbyObjectServices = jsonArray.optJSONObject(i);

                        servicelist.service_img                = NearbyObjectServices.optString("FilePath");
                        servicelist.service_id                 = NearbyObjectServices.optString("category_id");
                        servicelist.service_name               = NearbyObjectServices.optString("category_name");

                        allServicesList.add(servicelist);
                    }
                }
            }else {
                status_code = jsonObject.optString("status_code");
                message = jsonObject.optString("message");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}

