package com.wip.quickcare.fcm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

public class SharedPreferences {
    private static String val;
    public static String getVal() {
        return val;
    }

    public static void setVal(String val) {
        SharedPreferences.val = val;
    }


    public static void setFirstTimeUser(Activity context, String value) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("firstTimeUser", value);
        prefsEditor.commit();
    }

    public static String getFirstTimeUser(Activity context) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String val = mPrefs.getString("firstTimeUser", "0");
        return val;
    }

    public static void setUserLogin(Activity context, String username, String pass){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("username", username);
        prefsEditor.putString("pass", pass);
        prefsEditor.commit();
    }

    public static String getUsername(Activity context){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String val = mPrefs.getString("username", "");
        return val;
    }

    public static String getPass(Activity context){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String val = mPrefs.getString("pass", "");
        return val;
    }

    public static void clearUserLoginDetail(Activity context) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.remove("username");
        prefsEditor.remove("pass");
        prefsEditor.commit();
    }


    public static String getFCMRecod(Context context)
    {

        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String val = mPrefs.getString("KEY_Recod", "");
        Log.e("fcm_get", val);
        return val;

    }
    public static void setFCMRecod(Context context, String m)
    {
        Log.e("fcm_set", m);
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("KEY_Recod", m);
        prefsEditor.commit();
    }


    public static void setUserId(Activity context, String user_id) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("user_id", user_id);
        prefsEditor.commit();
    }

    public static String getUserId(Context context){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        val = mPrefs.getString("user_id", "");
        return val;
    }


    public static void setNotificationMessage(Context context, String notification_message) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("noty_mess", notification_message);
        Log.e("noty_mess11",notification_message);
        prefsEditor.commit();
    }

    public static String getNotificationMessage(Context context){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        val = mPrefs.getString("noty_mess", "");
        return val;
    }


    public static void setNotificationQuotationId(Context context, String quotation_id) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("quotation_id", quotation_id);
        Log.e("iddd",quotation_id);
        prefsEditor.commit();
    }

    public static String getNotificationQuotationId(Context context){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        val = mPrefs.getString("quotation_id", "");
        return val;
    }



    // clear all shared preference data after logout
    @SuppressLint("ApplySharedPref")
    public static void clearSession(Activity context){
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        mPrefs.edit().remove("username").commit();
        mPrefs.edit().remove("pass").commit();
        mPrefs.edit().remove("user_id").commit();
        mPrefs.edit().remove("viewings").commit();
    }

    @SuppressLint("ApplySharedPref")
    public static void setFreeViewings(Context context, String viewings) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("viewings", viewings);
        prefsEditor.commit();
    }

    public static String getFreeViewings(Context context) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String val = mPrefs.getString("viewings", "0");
        return val;
    }
}
