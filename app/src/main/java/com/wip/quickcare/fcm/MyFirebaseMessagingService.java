package com.wip.quickcare.fcm;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import android.text.Html;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wip.quickcare.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String title ;
    String message ;
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String message1="";
    private NotificationUtils notificationUtils;

    @Override
    public void onNewToken(String mToken) {
        super.onNewToken(mToken);
        Log.e("TOKEN",mToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Context context = this;

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            Log.e(TAG, "Notification Body1: " + remoteMessage.getData().toString());

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                public void run() {

                    if (remoteMessage.getNotification().getBody().contains("is the OTP with our Engineers")) {
                       /* Intent intent = new Intent("NOTIFICATION_LOCAL_BROADCAST");
                        intent.putExtra("message", remoteMessage.getNotification().getBody());
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);*/
                       // SharedPreferences.setNotificationMessage(context,"");
                        handleNotification1(remoteMessage.getNotification().getBody());

                        Log.e("Hahaha","Hahaha");
                    } else {
                        Log.e("Haha123","Haha123");
                       /* handleNotification(remoteMessage.getNotification().getBody());*/
                        handleNotification1(remoteMessage.getNotification().getBody());
                       // handleNotification(context,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());

                        Log.e("message12", remoteMessage.getNotification().getTitle());
                    }
                }
            });

            //  handleNotification(remoteMessage.getNotification().getBody());
        }

    }



    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
       // Log.d("fcm", "onMessageSent: message sent"+ s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
       // Log.d("fcm", "onSendError: erroe");
    }

    private void handleNotification1(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("message", message);

            String msg1 = String.valueOf(Html.fromHtml(message));
            showNotificationMessage(getApplicationContext(), msg1, resultIntent);

        }else{
            // If the app is in background, firebase itself handles the notification
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("message", message);

            String msg1 = String.valueOf(Html.fromHtml(message));
            showNotificationMessage(getApplicationContext(), msg1, resultIntent);
        }
    }


    private void handleNotification(Context context, String id, String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            Log.e("bb","hashsha");
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("message", message);
            showNotificationMessage(getApplicationContext(),message, resultIntent);

        }else{
            Log.e("aa","hashsha");
            // If the app is in background, firebase itself handles the notification
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("message", message);
            showNotificationMessage(getApplicationContext(),message, resultIntent);

        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

                    if (json.has("message")) {

                         title = json.getString("body");
                         message = json.getString("message");

                       if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                            resultIntent.putExtra("message", message);
                            Log.e("all 111",message);


                            showNotificationMessage(getApplicationContext(), message, resultIntent);
                            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                               Log.e("all 1111",message);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                            // play notification sound
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                        } else {
                            // app is in background, show the notification in notification tray
                            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                            resultIntent.putExtra("message", message);
                            Log.e("all 222",message);
                             showNotificationMessage(getApplicationContext(), message, resultIntent);
                        }

                    }

/*

                    if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                        Intent pushNotification = new Intent(this,Notification.class);
                        pushNotification.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        pushNotification.putExtra("message", message);
                        Log.e("all 111111",message);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                        startActivity(pushNotification);

                    } else {
                        // app is in background, show the notification in notification tray
                        Intent resultIntent = new Intent(getApplicationContext(), MobileActivity.class);
                        resultIntent.putExtra("message", message);
                        Log.e("all 2222222",message);
                         showNotificationMessage(getApplicationContext(), message, resultIntent);

                    }
*/


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception:11111111 " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String message, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        Log.e("innoty","in noty");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(message, intent);
    }

}


