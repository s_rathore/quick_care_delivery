package com.wip.quickcare.fcm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;

import androidx.legacy.content.WakefulBroadcastReceiver;

import com.wip.quickcare.MainActivity;


public class FirebaseBackgroundService extends WakefulBroadcastReceiver {

    private static final String TAG = "FirebaseService";
    private NotificationUtils notificationUtils;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "I'm in!!!");

        if (intent.getExtras() != null) {
            for (String key : intent.getExtras().keySet()) {
                Object value = intent.getExtras().get(key);


                if(key.equalsIgnoreCase("gcm.notification.body") && value != null) {
                    Bundle bundle = new Bundle();

                    Intent backgroundIntent = new Intent(context, MainActivity.class);
                    bundle.putString("mess", value + "");
                    Log.e("Mess",value+"");

                   /* SharedPreferences.setNotificationMessage(context,value.toString());
                    backgroundIntent.putExtras(bundle);
                    context.startService(backgroundIntent);*/

                    handleNotification1(value.toString(),context);
                }

                else if(key.contains("gcm.notification.title")&& value != null) {

                    Log.e("MessTitle",value+"");

                    /*Bundle bundle = new Bundle();
                    Intent backgroundIntent = new Intent(context, ServiceNotification1.class);
                    bundle.putString("push_message", value + "");
                    SharedPreferences.setNotificationBookingId(context,value.toString());
                    Log.e("Val",value+"");
                    backgroundIntent.putExtras(bundle);
                    context.startService(backgroundIntent);*/
                }
            }
        }
    }


    private void handleNotification1(String message, Context context) {
        if (NotificationUtils.isAppIsInBackground(context)) {
            Log.e("message3555", message);
            Intent resultIntent = new Intent(context, MainActivity.class);
            resultIntent.putExtra("message", message);
            showNotificationMessage(context,message, resultIntent);
        }


       /* else {
            Intent resultIntent = new Intent(context, UserDetailsActivity.class);
            resultIntent.putExtra("message", message);
            showNotificationMessage(context,message, resultIntent);
            Log.e("message11", message);
        }
*/

    }


    private void showNotificationMessage(Context context, String message, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        Log.e("innoty11","in noty");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(message, intent);
    }


}