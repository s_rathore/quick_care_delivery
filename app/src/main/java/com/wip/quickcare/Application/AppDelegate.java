package com.wip.quickcare.Application;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.wip.quickcare.volley.LruBitmapCache;
import com.wip.quickcare.volley.VolleyMultipartRequest;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class AppDelegate extends Application implements Foreground.Listener {

    private static final int VOLLEY_TIMEOUT_MULTIPART = 30000;

    /**
     * This interface is used to handle response
     */
    public interface ResponseHandler {
        void Success(Object jsonObject);
        void Error(VolleyError volleyError);
    }

    public static final String TAG = AppDelegate.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private ImageLoader mImageLoader;

    private static AppDelegate mInstance;

    private Foreground.Binding listenerBinding;

    @Override
    public void onCreate() {
        super.onCreate();

        listenerBinding = Foreground.get(this).addListener(this);

        if (mInstance == null){
            mInstance = this;
        }

        if (mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        if(mImageLoader == null){
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }

    }


    /**
     * Delegate Method to get the state of application
     * application is in forground
     * application is in background
     */

    @Override
    public void onBecameForeground() {

    }

    @Override
    public void onBecameBackground() {

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        listenerBinding.unbind();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    /**
     * This method is used to get instance of Application
     * @return
     */
    public static synchronized AppDelegate getInstance() {
        return mInstance;
    }

    /**
     * This method is used to get instance of RequestQueue
     * @return
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * This is used to Initialize ImageLoader to load images
     * @return
     */
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    /**
     * This method is used to load Image from url in imageview
     * @param URL
     * @param imageView
     */
    public void loadImageFromURLInImageView(String URL, final ImageView imageView) {
        if (mImageLoader == null){
            getImageLoader();
        }
        else {
            mImageLoader.get(URL, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }
    }


    /**
     * This method is used to add request in RequestQueue
     * @param req request object
     * @param tag request tag
     * @param <T>
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty

        if(mInstance!=null) {
            req.setShouldCache(false);
            req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
            getRequestQueue().add(req);
        }
    }


    /**
     * This is get request to fetch data from url
     * @param path String :- This is url of tha data
     * @param tag String :- This is used to set tag for request
     * @param handler ResponseHandler
     */
    public void GetRequest(String path, String tag, final ResponseHandler handler) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        if (jsonObject != null) {
                            handler.Success(jsonObject);
                        } else {
                            VolleyError volleyError = new VolleyError("Json Error");
                            showError(volleyError, handler);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        handler.Error(volleyError);
                        showError(volleyError, handler);

                    }
                });


        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(VOLLEY_TIMEOUT_MULTIPART, 0, 1.0f));
        getInstance().addToRequestQueue(jsonObjectRequest, tag);

    }


    public void multipartPostRequestLogin(String path, Map<String, String> paramsmap, String tag, final ResponseHandler handler) {
        final Map<String, String> map = paramsmap;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(1, path, new Response.Listener<NetworkResponse>() {
            public void onResponse(NetworkResponse response) {
                try {

                  /*  Log.e("Register_result_login", String.valueOf(response.data));

                    JSONObject result = new JSONObject(new String(response.data));*/

                    String resultResponse = new String(response.data);
                    Log.e("resultResponse", resultResponse.toString());
                    // try {
                    String jsonString =
                            new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    Log.e("response_service",jsonString);
                    JSONObject result = new JSONObject(jsonString);



                    if (result != null) {
                        Log.e("Register_result", result.toString());
                        handler.Success(result);
                    }
                } catch (Exception e) {
                    Log.e("Register_Exception", "Register_Exception");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                handler.Error(error);
                AppDelegate.this.showError(error, handler);
            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }

        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(VOLLEY_TIMEOUT_MULTIPART, 1, 1.0f));
        getInstance().addToRequestQueue(multipartRequest, tag);
    }


    public void multipartPostRequest(String path, Map<String, String> paramsmap, byte[] userimage, String tag, final ResponseHandler handler) {

        final Map<String, String> map = paramsmap;
        final byte[] bArr = userimage;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(1, path, new Response.Listener<NetworkResponse>() {
            public void onResponse(NetworkResponse response) {

                try {
                    JSONObject result = new JSONObject(new String(response.data));

                    if (result != null) {
                        handler.Success(result);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                handler.Error(error);
                AppDelegate.this.showError(error, handler);
            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }


            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap();
                params.put("user_image", new DataPart("image" + System.currentTimeMillis() + ".png", bArr, "image/png"));
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(VOLLEY_TIMEOUT_MULTIPART, 0, 1.0f));
        getInstance().addToRequestQueue(multipartRequest, tag);
    }



    public void multipartPostRequestArray(String path, Map<String, String> paramsmap, ArrayList<String> problem_image, String tag, final ResponseHandler handler) {

        final Map<String, String> map = paramsmap;


        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(1, path, new Response.Listener<NetworkResponse>() {
            public void onResponse(NetworkResponse response) {
                try {

                  //  Log.e("response",response.data+"");
                  //  JSONObject result = new JSONObject(new String(response.data));

                    String resultResponse = new String(response.data);
                    Log.e("resultResponse", resultResponse.toString());
                   // try {
                        String jsonString =
                                new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    Log.e("response_service",jsonString);
                        JSONObject result = new JSONObject(jsonString);


                    if (result != null) {
                        handler.Success(result);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                handler.Error(error);
                AppDelegate.this.showError(error, handler);
            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                for (int i=0;i<problem_image.size();i++) {
                    long t2 = System.currentTimeMillis();
                    String image=problem_image.get(i);
                    byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
                    params.put("problem_images["+i+"]", new DataPart("problem_images"+String.valueOf(t2)+".png", encodeByte, "image/png"));
                }
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(VOLLEY_TIMEOUT_MULTIPART, 0, 1.0f));
        getInstance().addToRequestQueue(multipartRequest, tag);
    }



    /**
     * This method is used to show error from response
     * @param volleyError
     * @param responseHandler
     */
    private void showError(final VolleyError volleyError, final ResponseHandler responseHandler){
        NetworkResponse networkResponse = volleyError.networkResponse;


        if (volleyError instanceof NoConnectionError ||volleyError instanceof TimeoutError) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "No Internet Connection, Please check your internet connection to use this application", Toast.LENGTH_SHORT).show();
                    responseHandler.Error(volleyError);
                }
            });
        }
        else  if (volleyError instanceof ServerError){
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Log.e("ServerError",volleyError.getLocalizedMessage() + volleyError.networkResponse.statusCode);
                    responseHandler.Error(volleyError);
                }
            });
        }
        else if (volleyError instanceof AuthFailureError) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Log.e("AuthFailureError","AuthFailureError");
                    responseHandler.Error(volleyError);
                }
            });
        }
        else if (volleyError instanceof NetworkError) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Log.e("NetworkError","NetworkError");
                    responseHandler.Error(volleyError);
                }
            });
        }
        else if (volleyError instanceof ParseError) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Log.e("ParseError","ParseError");
                    responseHandler.Error(volleyError);
                }
            });
        }
        else {
            responseHandler.Error(volleyError);
        }
    }

}
