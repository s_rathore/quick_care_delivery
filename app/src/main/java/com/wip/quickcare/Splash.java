package com.wip.quickcare;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.wip.quickcare.fcm.Config;
import com.wip.quickcare.fcm.NotificationUtils;
import com.wip.quickcare.fcm.SharedPreferences;

public class Splash extends AppCompatActivity /*implements Animation.AnimationListener*/ {
    Context context;
    ImageView oar2_t, oar3_t, oar4_t, oar5_t;
    ImageView oar1, oar2, oar3, oar4, oar5;
    Animation animMove_Logo, animMove_Logo2, animMove_Logo3, animMove_Logo4, animMove_icon_layout, animBlink_Logob, animMove_Logo5;
    String refreshedToken = "";
    private static final String TAG = Splash.class.getSimpleName();
    private android.os.Handler handler = new android.os.Handler();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    RuntimePermissionManagerNew runtimePermissionManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        runtimePermissionManager = new RuntimePermissionManagerNew(Splash.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            runtimePermissionManager.areAllPermissionsGranted();
        }
        //  PrefernceSettings.openDataBase(this);
        context = this;

        initFlip();
    }


    private void initFlip() {

        FirebseToken();
        new Thread(new Runnable() {
            public void run() {
                try {

                    Thread.sleep(1500);
                    displayFirebaseRegId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    Log.e("Noty_found", "noty found");
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    // displayFirebaseRegId();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    Log.e("Noty_found11", "noty found11");
                    String message = intent.getStringExtra("message");
                    // Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

    }

    public void onStart() {
        super.onStart();


        handler.postDelayed(new Runnable() {
            public void run() {
                            startActivity(new Intent(Splash.this, MainActivity.class));
                            finish();
                    }

        }, 3000);

    }


    private void FirebseToken() {
        try {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Splash.this, instanceIdResult -> {
                refreshedToken = instanceIdResult.getToken();
                Log.e("newToken", refreshedToken);
                android.content.SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                android.content.SharedPreferences.Editor editor = pref.edit();
                editor.putString("regId", refreshedToken);
                Log.e("regid", refreshedToken);
                editor.commit();
            });


        } catch (Exception e) {
            Log.d("newToken123", "problem");
            e.printStackTrace();
        }
    }


    private void displayFirebaseRegId() {
        //  SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        //  String regId = pref.getString("regId", null);

        Log.e("refToken", refreshedToken);

        String regId = refreshedToken;
        Log.e(TAG, "Firebase reg id: " + regId);
        if (!TextUtils.isEmpty(regId)) {
            SharedPreferences.setFCMRecod(this, regId);
            Log.e("11111111111111:", "Push notification: " + regId);
            //Toast.makeText(getApplicationContext(), "Push notification: " + regId, Toast.LENGTH_LONG).show();
        } else {
            Log.e("Firebase reg id:", "Firebase Reg Id is not received yet!");
            //Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}